# introduction du rapport :

La fabrication additive, également connue sous le nom d'impression 3D, a révolutionné l'industrie manufacturière en offrant des possibilités sans précédent en termes de conception, de personnalisation et de rapidité de production. Parmi les différents matériaux utilisés en fabrication additive, le POM et le PLA sont deux polymères très répandus, chacun apportant ses propres avantages et caractéristiques uniques.

Le POM, également appelé Delrin ou acétal, est un thermoplastique très apprécié pour sa résistance mécanique élevée, sa faible friction et sa stabilité dimensionnelle. Ces propriétés en font un choix privilégié dans les applications nécessitant des pièces de haute performance, notamment dans l'industrie automobile, l'aérospatiale et l'électronique.

Le PLA, quant à lui, est un bioplastique biodégradable dérivé de ressources renouvelables telles que l'amidon de maïs ou la canne à sucre. Sa popularité croissante est attribuée à son caractère écologique, sa facilité d'impression et son large éventail d'applications allant de la fabrication de jouets à la prototypage rapide.

L'une des avancées les plus prometteuses en fabrication additive est la possibilité d'imprimer en multimatériaux, permettant ainsi la création de pièces complexes et fonctionnelles avec des propriétés spécifiques, en combinant les avantages de différents matériaux. Cette approche a ouvert la voie à de nouvelles possibilités dans diverses industries, en particulier dans les secteurs nécessitant des propriétés mécaniques, thermiques ou électriques spécifiques pour des performances optimales.

Cependant, l'impression multimatériaux présente également son lot de défis techniques, et l'un des problèmes les plus courants est le warping (ou déformation par rétraction). Le warping se produit lorsque les pièces imprimées en 3D se refroidissent et se contractent inégalement, entraînant une déformation indésirable de la géométrie de la pièce et affectant sa précision dimensionnelle et sa qualité finale.

Lorsque l'on imprime avec des matériaux tels que le POM et le PLA, qui ont des coefficients de rétraction différents, le problème du warping est encore plus prononcé. Les différences de contraintes internes entre les matériaux peuvent provoquer des tensions résiduelles importantes, ce qui entraîne une déformation excessive de la pièce. La présence de plusieurs matériaux dans une même impression complique davantage la prévision du comportement de rétraction, rendant la simulation d'autant plus cruciale pour éviter les défauts potentiels.

Dans ce rapport, nous explorerons donc en détail la fabrication additive en multimatériaux avec du POM et du PLA, en mettant l'accent sur les défis liés au warping. Nous examinerons les approches de simulation actuellement disponibles pour anticiper et minimiser les effets du warping, ainsi que les méthodes de conception et de préparation des fichiers pour optimiser la qualité des impressions. De plus, nous étudierons les solutions expérimentales mises en œuvre par les fabricants pour atténuer le warping, telles que le contrôle de la température de la chambre d'impression, l'utilisation de supports spécifiques et l'ajustement des paramètres d'impression.


# Chapitre 1 : présentation de la société 

Au départ, il peut sembler un peu complexe de parler de l’institution d’un point
de vue introductif, car l’université Unilasalle Amiens peut avoir une histoire assez
longue selon le point de départ de l’étude, c’est pourquoi, pour être honnête, nous
parlerons de tout ce qui est lié de près ou de loin. Nous commencerons par une
perspective historique en parlant de l’origine des universités Lasalliennes, puis de
l’entité universitaire Unilasalle, et enfin, nous aborderons spécifiquement Unilasalle
Amiens.


## 1. Histoire : 

### 1.1. Naissance des institutions de UniLaSalle :

Le réseau d’institutions de La Salle se distingue en tant que réseau international
d’universités et d’écoles lasalliennes qui partagent une tradition éducative basée sur
les valeurs lasalliennes. Ces institutions se concentrent sur la formation intégrale des
étudiants et la promotion de la justice sociale.
Pour comprendre en détail ces institutions, il est important de remonter un peu dans
le passé et d’explorer leur origine et les principes d’enseignement qui les caractérisent.
La Salle a une histoire riche et significative dans le domaine de l’éducation, fondée par
Saint Jean-Baptiste de La Salle, qui a créé l’Institut des Frères des Écoles Chrétiennes
au XVIIe siècle. Saint Jean-Baptiste de La Salle croyait en la fourniture d’une éducation
de qualité à tous, en particulier à ceux qui étaient socialement défavorisés.
De nos jours, le réseau des universités La Salle s’étend dans plusieurs pays à travers le
monde, offrant une large gamme de programmes académiques dans diverses disciplines. Chacune de ces universités au sein du réseau de La Salle a sa propre approche et
spécialités, s’adaptant aux besoins et exigences de son environnement local.


<figure>
 <img src="images/jean.png" width="300" />
 </figure>

La première école lasallienne a été fondée à Reims, en France, en 1680. Cette école
a posé les bases de ce qui allait devenir l’Institut des Frères des Écoles Chrétiennes,
une congrégation religieuse fondée par La Salle dans le but de poursuivre sa mission
éducative. À partir de cette première école, de nombreuses écoles lasalliennes ont
été établies en France, puis dans d’autres pays, étendant ainsi leur action éducative à
travers le monde.
L’expansion de ces institutions à travers le monde a été impressionnante, car le réseau
lasallien d’universités et d’écoles s’est établi dans de nombreux pays sur tous les
continents. La vision de Saint Jean-Baptiste de La Salle de fournir une éducation de
qualité à tous, en particulier à ceux qui sont socialement défavorisés, a conduit à leur
fondation dans divers environnements culturels et sociaux.
En reconnaissance de sa contribution à l’éducation, Saint Jean-Baptiste de La Salle a
été canonisé en tant que saint par l’Église catholique en 1900. Son héritage et sa vision
pédagogique continuent d’influencer l’éducation jusqu’à ce jour, avec une focalisation
sur la formation intégrale des étudiants et un engagement envers la justice sociale.

### 1.2. Unilasalle :

Unilasalle est une institution d’enseignement reconnue à l’échelle internationale qui
fait partie du réseau lasallien d’universités et d’écoles. Elle a été fondée dans le but de
fournir une éducation de qualité et de former des professionnels compétents dans
divers domaines du savoir.

La fondation d’Unilasalle remonte à l’année 1843, lorsque la première université de
La Salle a été établie à Reims, en France. Cette institution a été créée grâce à la vision
et à l’engagement des Frères des Écoles Chrétiennes, qui cherchaient à offrir une
éducation de qualité à des jeunes de tous horizons sociaux.

<figure>
 <img src="images/salle.png" width="300" />
 </figure>

Depuis lors, Unilasalle a connu une croissance et une expansion, établissant plusieurs campus dans différentes régions de France et dans d’autres pays. L’institution s’est affirmée comme une référence dans l’enseignement supérieur, offrant une large gamme de programmes académiques dans des domaines tels que l’ingénierie, les sciences agricoles, les sciences sociales, la gestion d’entreprise, le tourisme et bien d’autres disciplines.

### 1.3  Unilasalle Amiens :


Unilasalle Amiens est un établissement d’enseignement situé dans la ville d’Amiens, en France. À l’origine connue sous le nom d’ESIEE Amiens, l’institution a connu une transition pour faire partie du réseau Unilasalle.
ESIEE Amiens a été fondée en 1992 et s’est spécialisée dans l’enseignement de l’ingénierie dans les domaines de l’électronique, de l’informatique et des télécommunications.
Pendant de nombreuses années, l’institution s’est distinguée par sa qualité académique et sa capacité à former des professionnels compétents dans le domaine de l’ingénierie.
En 2019, ESIEE Amiens a rejoint Unilasalle, un réseau renommé d’universités et d’écoles lasalliennes. Cette transition a permis à ESIEE Amiens de bénéficier de l’expérience et des ressources d’Unilasalle, tout en s’alignant sur les valeurs lasalliennes et en élargissant son offre académique.
Unilasalle Amiens, en tant que partie d’Unilasalle, propose désormais une large gamme de programmes académiques dans diverses disciplines, notamment l’ingénierie, les sciences agricoles, les sciences sociales, la gestion d’entreprise et le tourisme.
L’institution se concentre sur la fourniture d’une éducation intégrale et de qualité, en combinant théorie et pratique, tout en promouvant la responsabilité sociale et le développement durable.

<figure>
 <img src="images/salleamiens.png" width="300" />
 </figure>


La transition d’ESIEE Amiens à Unilasalle a permis à l’institution de renforcer sa position dans le domaine de l’éducation et d’élargir sa portée, offrant aux étudiants une expérience académique enrichissante et les préparant aux défis du monde du travail actuel.


## 2. L’université:

### 2.1.  Missions : 

La mission d’Unilasalle Amiens est de former des professionnels compétents, socialement engagés et responsables, en leur offrant une éducation de qualité basée sur les valeurs lasalliennes. L’institution vise à promouvoir le développement intégral des étudiants, en favorisant leur épanouissement personnel, académique et professionnel.

### 2.2. Visions : 

Sa vision est d’être reconnue comme une institution d’enseignement supérieur de référence, se distinguant par son excellence académique, son engagement envers la responsabilité sociale et son impact positif sur la société. L’institution aspire à former des leaders capables de contribuer au progrès social, économique et environnemental,
en accordant une attention particulière aux défis de la durabilité et de la justice sociale.

### 2.3. Valeurs :

Ses valeurs principales incluent la solidarité, la qualité, l’éthique, l’engagement, l’innovation et la diversité. Ces valeurs sont intégrées dans tous les aspects de la vie universitaire, guidant les actions et les décisions de l’institution.

### 2.4. La responsabilité sociale et le développement durable :

Unilasalle Amiens est une institution qui accorde une grande importance à l’environnement social et au développement durable. L’institution met en œuvre diverses initiatives et programmes visant à sensibiliser les étudiants aux enjeux sociaux et environnementaux, ainsi qu’à promouvoir des pratiques responsables.
L’institution encourage la responsabilité sociale à travers des projets communautaires et des collaborations avec des organisations locales. Les étudiants sont encouragés à participer à des initiatives bénévoles et à des actions caritatives, contribuant ainsi à améliorer la société et à soutenir les communautés locales.
En ce qui concerne le développement durable, Unilasalle Amiens adopte des pratiques respectueuses de l’environnement dans ses opérations quotidiennes. Elle encourage l’utilisation efficace des ressources, la réduction des déchets et la promotion de modes de vie durables. L’institution cherche également à développer la recherche et l’innovation dans le domaine de la durabilité, en encourageant les étudiants à participer à des projets de recherche liés à l’environnement et aux solutions durables.

### 2.5.  Les chiffres :

Avec plus de 650 étudiants, un taux de placement de 100% en moins de 6 mois après l’obtention du diplôme, 25 nationalités différentes et un revenu moyen annuel de plus de 36 000 euros, Unilasalle Amiens est l’une des institutions les plus réputées de sa ville et l’une des écoles les plus attrayantes.

<figure>
 <img src="images/chifre.png" width="300" />
 </figure>



# Chapitre 2 : La fabrication additive 

##  1. Introduction :

La fabrication additive, également connue sous le nom d'impression 3D, est une méthode de fabrication qui permet de produire des objets en ajoutant des couches successives de matériaux. Cette technologie est de plus en plus populaire en raison de sa grande flexibilité et de sa capacité à produire des pièces de formes et de tailles complexes avec une grande précision.

La fabrication additive a connu une croissance rapide ces dernières années et a trouvé des applications dans de nombreux secteurs, tels que l'aérospatiale, l'automobile, la médecine, l'architecture, la bijouterie, etc. Elle offre également des avantages tels que la réduction des déchets de production, la personnalisation des produits et la réduction des coûts de production.

Cependant, la fabrication additive présente également des défis tels que la limitation de la taille des pièces, la limitation des matériaux pouvant être utilisés et le coût initial élevé de l'investissement dans les machines et les logiciels de conception.

Dans ce rapport, nous allons explorer les différentes technologies de fabrication additive, leurs avantages et leurs inconvénients, ainsi que les applications de cette technologie dans différents secteurs. Nous allons également discuter des limites de la fabrication additive et des défis à relever pour son développement futur.



### 1.1. Historique : 

Les années 1980 et 1990 ont été marquées par les premières tentatives de développement de la fabrication additive, avec l'invention de la stéréolithographie, la fusion sur lit de poudre et le dépôt de fil fondu. Cependant, ces technologies étaient encore relativement limitées en termes de précision et de matières utilisables.

Au début des années 2000, la fabrication additive a commencé à gagner en popularité et à se développer pour devenir une technologie plus accessible et plus précise. Des machines plus grandes et plus rapides ont été développées, permettant la production de pièces plus grandes et plus complexes. De plus, l'arrivée de nouveaux matériaux tels que les métaux et les céramiques a permis d'élargir les applications de la fabrication additive.

Au cours de la dernière décennie, la fabrication additive est devenue encore plus avancée, avec l'émergence de nouvelles technologies telles que la photopolymérisation continue, la stéréolithographie à projection de masque et la fabrication additive basée sur le jet d'encre. Ces technologies ont permis une production plus rapide, plus précise et plus économique de pièces et de produits finis.

Aujourd'hui, la fabrication additive est largement utilisée dans de nombreux secteurs, notamment l'aérospatiale, l'automobile, la médecine, l'architecture, la bijouterie et même la production alimentaire. Elle offre des avantages tels que la personnalisation de masse, la réduction des déchets, la réduction des coûts de production et la capacité à produire des pièces complexes avec une grande précision. Cependant, la technologie continue d'évoluer et de relever des défis tels que la limitation des matériaux et la taille des pièces produites, ainsi que le coût initial élevé de l'investissement dans les machines et les logiciels de conception.

## 2.Principe de base de la fabrication additive :

Le processus commence par la création d'un modèle 3D numérique de l'objet souhaité, à l'aide d'un logiciel de modélisation 3D. Ce modèle est ensuite transféré à une imprimante 3D, qui utilise une variété de technologies telles que la fusion de dépôt de fil, la stéréolithographie, la frittage sélectif par laser, l'impression de jet de matière fondue, ou encore la bio-impression, pour créer l'objet couche par couche.

Les matériaux utilisés pour la fabrication additive varient selon la technologie utilisée et l'application finale de l'objet, mais peuvent inclure des plastiques, des métaux, des céramiques, des composites et même des tissus biologiques.

La fabrication additive est devenue une méthode de production populaire pour la création d'objets de formes complexes et personnalisés, ainsi que pour la production de prototypes rapides et de pièces uniques.

### 2.1. Les différentes techniques de fabrication additive : 

On peut considérer 7 procédés de fabrication additive selon une classification reconnue internationalement (ASTM F42) :


- __L’extrusion et dépôt de fil fondu :__ Il s'agit d'un procédé d'extrusion qui vient déposer, via une tête d'impression mobile, un matériau thermoplastique sur une surface. Le principe de dépôt de matière fondue permet de s'adresser à de nombreux matériaux thermoplastiques, biomatériaux ou composites (connaissant leur température de fusion), et rend possible l'impression simultanée de plusieurs matériaux pour un même objet. La plupart du temps, le matériau initial est conditionné sous forme de fil (c'est pourquoi on parle aussi de dépôt de fil fondu), mais cela peut aussi être des granulats ou des pâtes (plus ou moins liquides). Ce large éventail de matériau fait de ce procédé un bon candidat pour les industries agro-alimentaire ou pharmaceutique.

<figure>
 <img src="images/first.png" width="300" />
 </figure>


- __La photo-polymérisation d'une résine :__ Ce procédé est basé sur la polymérisation d'une résine photosensible, par une source lumineuse focalisée, couche par couche, dans un bain de résine. De part son principe, il permet la fabrication de pièces présentant des détails très fins, et un état de surface totalement lisse.
Ce procédé est particulièrement adapté pour l’impression de petit objet (voire nanométrique), nécessitant une grande finesse de détails (on peut citer des applications électroniques, dentaires, ou de joaillerie).
<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20091426.png" width="300" />
 </figure>


- __La fusion sur lit de poudre :__ C’est basé sur l'utilisation d'un LASER pour venir fusionner un matériau présent sous forme de poudre. Ces poudres peuvent être métalliques, plastiques ou céramiques. La poudre est déposée couche par couche (de l'ordre de 100 μm), et le laser focalisé permet le frittage localisé du matériau (impression par SLS (Selective Laser Sintering). En règle générale, le lit de poudre est maintenu à une température proche de la température de fusion, et le laser apporte l'énergie nécessaire pour passer au-dessus de cette température, et permettre le frittage de la poudre. L'homogénéité de la poudre est un critère important dans la conduite de ce procédé.
Lors de l'impression, la pièce est auto-portée dans le bac de poudre, ce qui permet de s'affranchir d'un matériau de support, et rend aussi possible la fabrication de plusieurs pièces indépendantes au cours d'une même impression.

<figure>
 <img src="images/third.png" width="300" />
 </figure>

- __La projection de matière :__ Le procédé d'impression par projection de matériaux consiste à pulvériser des gouttelettes de photo-polymères par des têtes d'impression qui viennent ainsi les déposer sur le plateau d'impression. Chaque couche est alors polymérisée par une source d'ultraviolet. La composition des gouttelettes peut varier en chaque point (comme lors d'une impression d'images par jet d'encre), ce qui offre une grande richesse de l'objet final du point de vue de sa composition matérielle, et donc de ses propriétés d'usage.
Dans l'ensemble, cette technique est surtout adaptée à du prototypage, car la tenue dans le temps des objets produits n'est pas très bonne.

<figure>
 <img src="images/fourth.png" width="300" />
 </figure>


- __La projection de liants :__ Il s'agit de la fabrication d'un objet par stratification d'une poudre. La poudre est progressivement encollée, couche par couche, à l'aide d'un liant projeté. Le liant peut être une colle extraforte, type cyano-crylate. Comme les colles peuvent être teintées, par mélange, on peut obtenir un objet multicolore.
Dans le cas de poudres métalliques, l'impression peut être suivie d'une étape critique de frittage, pour assurer la tenue mécanique de l'objet final.

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20091426.png" width="300" />
 </figure>


- __Le laminage, ou la stratification de matériau en feuilles :__ Ce procédé consiste à assembler entre-elles des feuilles de papier, de plastique ou de métaux. L'assemblage est réalisé par collage, ou par ultrasons. Après le dépôt d'une feuille, celle-ci est découpée à l'aide d'une lame ou d'un laser.

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20092137.png" width="300" />
 </figure>


- __Le dépôt direct de matière sous énergie concentrée :__ Le dépôt de matière sous énergie concentrée consiste à déposer du matériau (métal, céramique), le plus souvent à l'aide d'un bras robotisé à 4 ou 5 axes de liberté, dans un faisceau d'énergie (laser ou arc électrique) pour le faire fondre, comme lors d'une soudure. Cette étape de fabrication additive est rapide, et permet une grande liberté de mouvement de la tête d'impression, mais bien souvent grossière, elle peut alors être suivie par une étape classique d'usinage, notamment pour améliorer l'état de surface de l'objet fini, ce type de procédé est très peu répandu

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20092320.png" width="300" />
 </figure>

### 2.2. Comparaison avec les méthodes traditionnelles de fabrication :

La fabrication additive est une méthode de fabrication qui crée des objets en ajoutant des couches de matériau, généralement du plastique ou du métal, jusqu'à ce que la forme finale de l'objet soit obtenue. Les méthodes traditionnelles de fabrication, en revanche, consistent à retirer du matériau d'un bloc solide ou à le modeler en utilisant des outils tels que des moules ou des presses.

Voici quelques différences clés entre la fabrication additive et les méthodes traditionnelles de fabrication :

- Flexibilité de conception : la fabrication additive permet une grande flexibilité de conception car elle peut produire des formes très complexes qui ne sont pas possibles avec les méthodes traditionnelles de fabrication. Cela est particulièrement utile pour les prototypes et les pièces personnalisées.

- Coûts de production : la fabrication additive peut être plus coûteuse que les méthodes traditionnelles de fabrication, en particulier pour la production de grandes quantités de pièces. Cependant, elle peut être plus économique pour les petites quantités de pièces ou les prototypes.

- Délais de production : la fabrication additive peut prendre plus de temps que les méthodes traditionnelles de fabrication, en particulier pour la production de grandes quantités de pièces. Cependant, elle peut être plus rapide pour la production de petites quantités de pièces ou les prototypes.

- Qualité des pièces : la qualité des pièces produites par la fabrication additive peut varier en fonction du matériau utilisé et de la précision de l'imprimante 3D. Les méthodes traditionnelles de fabrication peuvent produire des pièces de qualité supérieure avec des propriétés mécaniques plus élevées.

Le choix entre la fabrication additive et les méthodes traditionnelles de fabrication dépend des besoins spécifiques de chaque projet, y compris la complexité de la conception, les quantités de production, les délais de production et les coûts de production.

---

##  3. Matériaux utilisés en fabrication additive : 

### 3.1. Types de matériaux utilisés en fabrication additive

Il existe plusieurs types de matériaux utilisés en fabrication additive, par exemple :

- Plastiques : Les plastiques sont les matériaux les plus couramment utilisés en fabrication additive. Ils sont souvent utilisés pour la création de prototypes, de pièces fonctionnelles et de jouets. Les plastiques couramment utilisés comprennent l'acrylonitrile butadiène styrène (ABS), le polycarbonate (PC) et le nylon.

<figure>
 <img src="images/filamentplastique.png" width="300" />
 </figure>

- Métaux : Les métaux sont également très populaires en fabrication additive. Les métaux couramment utilisés comprennent l'aluminium, le titane, l'acier inoxydable et le cuivre. Ils sont souvent utilisés pour la production de pièces aérospatiales, médicales et automobiles.
<figure>
 <img src="images/métaux.png" width="300" />
 </figure>

- Céramiques : Les céramiques sont utilisées pour la création d'objets tels que des pièces électroniques, des prothèses et des implants dentaires. Les céramiques couramment utilisées comprennent l'oxyde d'alumine et le nitrure de silicium.

<figure>
 <img src="images/céramique.png" width="300" />
 </figure>

- Composites : Les composites sont souvent utilisés pour la production de pièces résistantes et légères. Les composites couramment utilisés comprennent le carbone et la fibre de verre.

<figure>
 <img src="images/composite.png" width="300" />
 </figure>

- Biomatériaux : Les biomatériaux sont utilisés pour la production de prothèses et d'implants. Les biomatériaux couramment utilisés comprennent le polyéthylène haute densité et le polyéthylène glycol.

<figure>
 <img src="images/biomatériaux.png" width="300" />
 </figure>

- Autres matériaux : Il existe également d'autres matériaux utilisés en fabrication additive, tels que les cires, les mousses et les élastomères.

<figure>
 <img src="images/3ofthem.png" width="300" />
 </figure>



### 3.2. Propriétés des matériaux les plus couramment utilisés et leurs limitations : 

Chacun de ces matériaux a ses propres propriétés. Voici une liste des propriétés des matériaux les plus couramment utilisés en fabrication additive :

- Les plastiques : 

+Propriétés: résistance aux chocs, faible coût, large gamme de couleurs disponibles, facile à imprimer

-Limitations: faible résistance à la chaleur, faible résistance à l'usure, peut se déformer au fil du temps



- Les métaux :

+Propriétés: haute résistance, bonne résistance à la chaleur, faible déformation, bonne résistance à l'usure

-Limitations: coût élevé, nécessite une machine spécifique pour l'impression, peut être difficile à souder et à usiner

- Les céramiques : 

+Propriétés: haute résistance à la chaleur, haute résistance à l'usure, grande résistance à la corrosion, bonne résistance à la compression

-Limitations: fragilité, difficulté à travailler, coût élevé

- Les composites : 

+Propriétés: haute résistance, faible poids, grande résistance à la corrosion, faible coefficient de dilatation thermique

-Limitations: coût élevé, nécessite une machine spécifique pour l'impression, peuvent être difficiles à travailler et à assembler.

Chaque matériau utilisé en fabrication additive a ses propres avantages et limites, et le choix dépendra de l'application spécifique pour laquelle l'objet sera utilisé.



---

## 4. Applications de la fabrication additive : 

### 4.1. Domaines d'application de la fabrication additive:

Les domaines d'application de cette technologie sont en constante expansion, et incluent :

- Prototypage : La fabrication additive est souvent utilisée pour la fabrication rapide de prototypes, permettant ainsi aux entreprises de tester leurs produits plus rapidement et à moindre coût.

<figure>
 <img src="images/prototype.png" width="300" />
 </figure>

- Fabrication de pièces industrielles : La fabrication additive est utilisée dans l'industrie pour la production de pièces complexes et personnalisées, notamment dans l'aéronautique, l'automobile et le médical.

<figure>
 <img src="images/fabrication.png" width="300" />
 </figure>

- Bijouterie : La fabrication additive est utilisée pour la production de bijoux personnalisés et complexes.

<figure>
 <img src="images/bijoux.png" width="300" />
 </figure>


- Design : La fabrication additive permet aux designers de créer des objets uniques et complexes qui seraient autrement difficiles à produire.

<figure>
 <img src="images/design.png" width="300" />
 </figure>

- Architecture : La fabrication additive est utilisée pour la production de maquettes d'architecture, de modèles 3D et de composants de construction.

<figure>
 <img src="images/architecture.png" width="300" />
 </figure>

- Éducation : La fabrication additive est utilisée dans les écoles pour l'enseignement de la conception assistée par ordinateur (CAO) et pour permettre aux élèves de créer des objets en 3D.

<figure>
 <img src="images/education.png" width="300" />
 </figure>

- Art : La fabrication additive est utilisée par les artistes pour créer des œuvres d'art originales et complexes.

<figure>
 <img src="images/art.png" width="300" />
 </figure>

- Alimentation : La fabrication additive est utilisée pour créer des aliments personnalisés et complexes, tels que des chocolats, des pâtes et des pizzas.

<figure>
 <img src="images/alimentation.png" width="300" />
 </figure>

- Dentisterie : La fabrication additive est utilisée pour la production de prothèses dentaires et d'implants.

<figure>
 <img src="images/dentaire.png" width="300" />
 </figure>

- Mode : La fabrication additive est utilisée pour la production de vêtements et d'accessoires personnalisés et uniques.

<figure>
 <img src="images/mode.png" width="300" />
 </figure>


### 4.2. Avantages et inconvénients de l'utilisation de la fabrication additive :

La fabrication additive offre de nombreux avantages, mais elle présente également certains inconvénients. 

* Avantages :

Réduction des coûts : La fabrication additive peut réduire les coûts de production en éliminant les étapes de production traditionnelles telles que la découpe, le fraisage et le perçage.

Personnalisation : La fabrication additive permet de produire des produits uniques pour répondre aux besoins spécifiques des clients. Cela est particulièrement utile dans les domaines de la médecine et de la mode.

Flexibilité : La fabrication additive permet de réaliser rapidement des modifications et des ajustements de conception en fonction des besoins et des exigences de fabrication.

Réduction des déchets : La fabrication additive réduit les déchets de production car elle utilise uniquement les matériaux nécessaires pour créer l'objet.

* Inconvénients :

Coût initial élevé : L'achat d'une imprimante 3D peut être coûteux, ce qui peut être un obstacle à l'adoption de cette technologie pour les petites et moyennes entreprises.

Limites de la taille : Les imprimantes 3D ont des limites de taille de production, ce qui peut rendre difficile la fabrication de grands objets.

Qualité : La qualité des produits fabriqués par la fabrication additive peut être inférieure à celle des produits fabriqués de manière traditionnelle.

Limitations des matériaux : La plupart des imprimantes 3D ne peuvent utiliser qu'un nombre limité de matériaux.



## 5. Conception et modélisation pour la fabrication additive :

### 5.1 Conception pour la fabrication additive : 

La conception pour la fabrication additive (FA) est une approche de conception qui prend en compte les possibilités et les limitations de la technologie de FA pour produire des pièces optimisées et efficaces. La FA est une méthode de production qui permet de créer des pièces en ajoutant des couches successives de matériau, ce qui offre une grande flexibilité en termes de formes et de géométries possibles.

La conception pour la FA implique une approche différente de la conception traditionnelle, car elle offre la possibilité de créer des formes et des géométries qui ne seraient pas réalisables par les méthodes de production traditionnelles. Cela signifie que les designers et les ingénieurs peuvent concevoir des pièces qui sont plus légères, plus résistantes, plus fonctionnelles et plus complexes que celles qui seraient réalisables avec les méthodes de production traditionnelles.

Il est important de comprendre les caractéristiques mécaniques et physiques du matériau utilisé pour la FA, ainsi que les capacités de la technologie de FA spécifique qui sera utilisée pour produire la pièce. Cela permettra de concevoir des pièces optimisées pour la production par FA, tout en minimisant les coûts de production et en réduisant le temps de développement.

La conception pour la FA peut également impliquer l'utilisation de techniques de conception générative, qui permettent de générer automatiquement des formes et des géométries optimisées en fonction de critères spécifiques tels que la résistance, la légèreté ou la fonctionnalité.

En résumé, la conception pour la FA est une approche de conception qui prend en compte les possibilités et les limitations de la technologie de FA pour produire des pièces optimisées et efficaces. Il est important de comprendre les caractéristiques mécaniques et physiques du matériau utilisé et les capacités de la technologie de FA pour concevoir des pièces qui sont rentables et fonctionnelles.

### 5.2. Techniques de modélisation et de préparation des fichiers pour l'impression 3D : 

La modélisation et la préparation des fichiers pour l'impression 3D sont des étapes cruciales pour obtenir des pièces de qualité et fonctionnelles. Voici quelques techniques de modélisation et de préparation des fichiers pour l'impression 3D :

__1/__ Modélisation 3D : La modélisation 3D est la première étape de la préparation des fichiers pour l'impression 3D. Elle peut être réalisée à l'aide de logiciels de modélisation 3D tels que SolidWorks, Autodesk Inventor, Blender, Fusion 360, etc. Ces logiciels permettent de créer des modèles 3D à partir de zéro ou de numériser des objets existants à l'aide de scanners 3D.

__2/__ Optimisation du modèle : Une fois que le modèle 3D est créé, il est important de l'optimiser pour l'impression 3D. Cela implique de s'assurer que la géométrie de la pièce est adaptée à la technologie d'impression 3D utilisée. Par exemple, il peut être nécessaire de modifier la forme de la pièce ou d'ajouter des supports pour garantir une impression sans problème.

__3/__ Correction de fichiers : Les fichiers de modélisation peuvent contenir des erreurs ou des défauts qui peuvent empêcher l'impression. Il est donc important de corriger ces erreurs à l'aide de logiciels de réparation de fichiers tels que Netfabb, MeshLab, etc.

__4/__ Orientation et placement de la pièce : Une fois que le modèle est prêt, il est important de le placer correctement dans l'espace d'impression pour minimiser les supports et les erreurs. Il est également important d'orienter la pièce pour garantir que les surfaces imprimées soient lisses et esthétiques.

__5/__ Supports : Pour les pièces plus complexes, il peut être nécessaire d'ajouter des supports pour maintenir la pièce en place pendant l'impression. Les supports doivent être placés de manière à minimiser les surfaces de contact avec la pièce pour éviter les marques de supports.

__6/__ Exportation du fichier : Une fois que la pièce est prête pour l'impression 3D, il est important d'exporter le fichier dans un format compatible avec la technologie d'impression 3D utilisée, tel que STL, OBJ, etc.

La modélisation et la préparation des fichiers pour l'impression 3D impliquent une série d'étapes, notamment la modélisation 3D, l'optimisation du modèle, la correction de fichiers, l'orientation et le placement de la pièce, l'ajout de supports et l'exportation du fichier. Il est important de maîtriser ces étapes pour obtenir des pièces de qualité et fonctionnelles.


### 5.3. Paramètres d'impression et de post-traitement

Les paramètres d'impression 3D peuvent varier en fonction du matériau utilisé, de la conception de la pièce, de la précision requise, de la taille de la pièce et d'autres facteurs. Les paramètres clés comprennent :

- Température de l'extrudeur : cette température est cruciale pour faire fondre le matériau et le faire sortir de l'extrudeur de l'imprimante 3D. La température doit être réglée en fonction du matériau utilisé.

- Vitesse d'impression : la vitesse à laquelle la buse se déplace détermine la rapidité de l'impression. Une vitesse d'impression plus élevée peut réduire le temps d'impression, mais peut affecter la qualité de la pièce.

- Densité de remplissage : la densité de remplissage détermine la quantité de matériau utilisée à l'intérieur de la pièce imprimée. Une densité de remplissage plus élevée peut rendre la pièce plus solide, mais prendra plus de temps à imprimer.

- Epaisseur de couche : l'épaisseur de la couche détermine la résolution de la pièce imprimée. Une épaisseur de couche plus fine peut produire une pièce plus précise, mais prendra plus de temps à imprimer.

Le post-traitement de la pièce imprimée dépend également du matériau utilisé et de l'application de la pièce. Les techniques courantes de post-traitement comprennent :

- Élimination des supports : les supports utilisés pendant l'impression doivent être enlevés soigneusement pour éviter d'endommager la pièce. Cela peut être fait manuellement ou en utilisant des outils tels que des pinces ou des couteaux.

- Sablage : le sablage est une technique de polissage qui utilise des particules abrasives pour lisser la surface de la pièce imprimée.

- Recouvrement : le recouvrement est une technique de finition qui consiste à appliquer une couche de matériau sur la surface de la pièce pour la protéger et améliorer son apparence.

- Peinture : la peinture peut être utilisée pour améliorer l'apparence de la pièce et lui donner une finition plus professionnelle.


####  5.3.1. Imprimante E3D Tool Changer 

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20092457.png" width="300" />
 </figure>


Le changeur d'outil E3D est un système avancé pour les imprimantes 3D qui permet aux utilisateurs de changer les têtes d'outils et les extrudeurs pendant une impression. Il est conçu pour fournir une solution polyvalente et personnalisable pour les utilisateurs avancés qui cherchent à imprimer avec différents matériaux, couleurs ou têtes d'outils spécialisées.
Le système de changement d'outil est généralement constitué d'une base fixe, sur laquelle plusieurs têtes d'outils peuvent être installées. Chaque tête d'outil est équipée de son propre extrudeur, hotend et ventilateur, et peut être facilement attachée ou détachée de la base. Les têtes d'outils peuvent être utilisées pour imprimer avec différents matériaux, couleurs ou pour effectuer des tâches spécialisées telles que la gravure ou la découpe au laser.
Le changeur d'outil E3D est généralement contrôlé par un logiciel de gestion qui gère le mouvement et la position des têtes d'outils, ainsi que le changement de filaments et d'outils. Il peut également s'intégrer à des configurations logicielles et matérielles d'impression 3D existantes, ce qui en fait une solution personnalisable pour les utilisateurs avancés qui cherchent à exploiter pleinement leur imprimante 3D.

---

#### 5.3.2. L’influence des paramètres sur les résultats : 

__1er essai :__
<figure>
 <img src="images/secondfirst.png" width="300" />
 </figure>


Pour le premier essai on travaille avec le PLA en utilisant les paramètres mises par défaut pour ce type de matériaux, une température du buse à 190°C, la température du plateau est à 70°C, et une vitesse  d'avancement d'extrusion 120mm/s​
On remarque plusieurs défauts dans la pièce :

- Le stringing (les petits fils en passant d'un élément à un autre )

- L’imprécision des dimensions

- Details estompés 

- Sur-extrusion (rainures pleines du matériau) 

__2eme essai :__

Pour cet essai, on essaie de régler les paramètres afin de remédier le problème des détails estompés, l’imprécision des dimensions et la sur-extrusion, donc on change la valeur du débit, en réduisant le multiplicateur d’extrusion à 0.9

(Réglage Filament > Filament > Multiplicateur d’extrusion)

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20093643.png" width="300" />
 </figure>



Les détails ne sont toujours pas clairs, en revanche les dimensions sont plus précises et on remarque moins de sur-extrusion 

__3eme essai :__

On réduit encore le multiplicateur d’extrusion à 0.8

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20093829.png" width="300" />
 </figure>


La précision est bonne, les détails sont plus clairs mais ils restent toujours estompés 

__4eme essai :__

On réduit la vitesse d’extrusion:

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20094136.png" width="300" />
 </figure>


On fixe la largeur d’extrusion à 0.45 mm :

<figure>
 <img src="images/Capture%20d%E2%80%99%C3%A9cran%202023-04-11%20094243.png" width="300" />
 </figure>


<figure>
 <img src="images/capture.png" width="300" />
 </figure>

on vise le stringing et la sur-extrusion dans le prochain essai 

__5eme essai :__
je diminue le multiplicateur d'extrusion pour faire apparaitre l'ecriture davantage, mais sans créer des "Gaps" 

<figure>
 <img src="images/multiplicateur.png" width="300" />
 </figure>

j'augmente la longueur de la rétractation à 3.5 mm pour rémidier le stringing 

<figure>
 <img src="images/retractation.png" width="300" />
 </figure>


<figure>
 <img src="images/together.png" width="300" />
 </figure>

on a réussi à éliminer la sur extrusion mais on remarque toujours le "stringing"
__6eme essai :__

je diminue la température de l'éxtrusion

<figure>
 <img src="images/températures.png" width="300" />
 </figure>

j'augmente la longueur et la vitesse de rétractation  : 

<figure>
 <img src="images/vitesseretract.png" width="300" />
 </figure>

On utilise les ventilateurs pour le refroidissement 

<figure>
 <img src="images/cold.png" width="300" />
 </figure>

<figure>
 <img src="images/couple.png" width="300" />
 </figure>


Après plusieurs essais, pour remédier les problème du premier essai, j'ai réduit le débit en fixant le multiplicateur d'extrusion à 0.75, la vitesse d'extrusion à 80mm/s, la largeur d'extrusion à 0.45mm,  la température d'extrusion à 185°C, et j'ai augmenté la longueur de rétractation à 4 mm et la vitesse de rétractation à 120mm/s, et j'ai activé le refroidissement par ventilateur ​

on a obtenu une pièce avec :​

des dimensions  précises​

des détails plus clairs​

La sur-extrusion est éliminée ​

Moins du stringing 


 #### 5.3.3. Extrusion avec deux extrudeuses en PLA :

Le test de calibrage consiste à vérifier si les deux extrudeuses d'une imprimante 3D bi-extrusion fonctionnent correctement ensemble. Ce test est important car il assure que les deux extrudeuses déposent les matériaux de manière précise et coordonnée.

Ce test peut être effectué en imprimant un objet simple, comme un cube avec deux couleurs différentes. Pendant l'impression, les deux couleurs doivent être alignées avec précision et les transitions entre les deux couleurs doivent être nettes et uniformes. Si les deux extrudeuses ne sont pas coordonnées correctement, les transitions entre les deux couleurs peuvent être floues, ou les deux matériaux peuvent ne pas être déposés correctement ensemble.

<figure>
 <img src="images/Ka7labidha.png" width="300" />
 </figure>

---
#### 5.3.4. Impression 3D en multi-matériaux :

L'impression en multimatériaux est une technique d'impression 3D qui permet d'imprimer des pièces en utilisant plusieurs matériaux différents en même temps. Cette technique est souvent utilisée pour imprimer des pièces complexes avec des propriétés différentes dans différentes zones, ou pour créer des pièces avec des couleurs multiples.

Pour imprimer en multimatériaux, l'imprimante 3D doit avoir plusieurs extrudeurs qui peuvent déposer différents matériaux simultanément. Les extrudeurs peuvent être configurés pour imprimer des matériaux différents à des endroits différents de la pièce, ou pour mélanger différents matériaux ensemble pour créer des propriétés hybrides.

La configuration de l'imprimante pour l'impression en multimatériaux peut être complexe, car il est nécessaire prendre en compte les propriétés des différents matériaux utilisés, tels que la température de fusion et le débit, pour assurer une impression réussie.

L'impression en multimatériaux peut être utilisée dans de nombreux domaines, tels que la fabrication de pièces automobiles, les prothèses médicales personnalisées, les pièces d'aéronefs, les jouets, etc. Cette technique offre une grande flexibilité pour la création de pièces avec des propriétés personnalisées, tout en permettant d'économiser du temps et de l'argent par rapport aux techniques traditionnelles de fabrication


 __les paramétres de PLA__

- température de la buse = 195°C
- température de plateau = 70°C
- vitesse d'impression = 60 mm/s
- retractation et sa viteese = 6 mm, 120mm/s
- refroidissement par ventilateur 

__les paramètres de POM__

- température de la buse = 250°C
- température de plateau = 100°C
- vitesse d'impression = 20 mm/s
- retractation et sa viteese = 3.5 mm, 60mm/s
- refroidissement par la température d'air ambiant 


<figure>
 <img src="images/theltha.png" width="300" />
 </figure>


avec 3 couche de PLA et ue couche de POM chacune de 0.2mm  d'épaisseur, on visualise le phénomène du warping, la pièce se décole du plateau ce qui empeche l'impression 3D. 


# Chapitre 3 : simulation par éléments finis (COMSOL) 

## 1. Introduction : 

### 1.1.  L’importance de la simulation par éléments finis dans l'impression 3D :

La simulation de fabrication additive est un outil essentiel pour optimiser le processus de fabrication additive et produire des pièces de haute qualité tout en réduisant les coûts et les risques pour la santé et la sécurité. Elle permet de prévoir les problèmes potentiels dans le processus de fabrication additive avant la phase de production, ce qui permet de réduire les déchets, d'économiser du temps et de réduire les coûts associés aux tests physiques. La simulation permet également d'optimiser les paramètres du processus de fabrication additive pour produire des pièces de meilleure qualité avec moins de défauts, tels que des pores, des fissures, des déformations, etc. De plus, la simulation permet d'identifier les risques potentiels associés à la production de pièces en utilisant des matériaux potentiellement dangereux, ce qui permet de mettre en place des mesures de prévention et de protection appropriées pour les travailleurs. Enfin, la simulation permet de tester différents matériaux et paramètres de processus de fabrication additive pour développer de nouveaux matériaux et de nouveaux processus avec des propriétés améliorées ou innovantes, ainsi que de personnaliser les produits pour répondre aux besoins spécifiques des clients ou des applications. 

la simulation de fabrication additive est un outil incontournable pour assurer un processus de production de qualité, économique et sécurisé, ainsi que pour développer de nouvelles solutions innovantes et personnalisées.

### 1.2.  Le problème du warping dans l'impression 3D et son impact :

Le warping, également connu sous le nom de gauchissement ou déformation, est l'un des problèmes courants rencontrés lors de l'impression 3D, en particulier pour les pièces de grande taille ou avec certaines géométries spécifiques. Le warping se produit lorsque des contraintes internes se développent pendant le processus d'impression, entraînant une déformation indésirable de la pièce. Ce problème peut avoir un impact significatif sur la qualité et la fonctionnalité de l'objet imprimé,ses impacts les plus importants sont :

Déformation de la géométrie : Le warping peut entraîner une déformation de la géométrie de la pièce, ce qui signifie que la forme finale ne correspondra pas à celle prévue dans le modèle 3D. Cela peut rendre la pièce inutilisable pour l'application prévue ou entraîner des problèmes d'ajustement si elle doit être assemblée avec d'autres composants.

Problèmes d'adhérence au plateau d'impression : c'est un soulèvement des bords de la pièce par rapport au plateau d'impression. Cela peut compromettre l'adhérence de la première couche de matériau à la surface d'impression, ce qui peut perturber l'ensemble du processus d'impression et conduire à un échec de l'impression.

Finition de surface médiocre : Lorsque le warping se produit, la pièce peut se déformer pendant l'impression, entraînant des irrégularités sur la surface de l'objet. Cela peut rendre la surface rugueuse, inégale ou présenter des défauts, ce qui affecte l'esthétique de la pièce et sa fonctionnalité.

Décalage des dimensions : il sagit d'un rétrécissement ou un agrandissement de certaines parties de la pièce par rapport aux dimensions prévues. Cela peut rendre la pièce inexacte par rapport aux spécifications et aux tolérances requises pour son application.

Risque de défaillance mécanique : Lorsque le warping affecte la structure interne de la pièce, cela peut affaiblir sa résistance mécanique et sa durabilité. Cela peut entraîner une défaillance prématurée de la pièce sous des charges ou des contraintes normales.

## 2. Modélisation de l'impression 3D de la POM et du PLA :

La mise en place de la simulation a consisté à définir la géométrie de la plaque d'impression, la couche de PLA et
Couche POM dans COMSOL. Propriétés des matériaux telles que les propriétés mécaniques du PLA
et POM ont été pris en compte, ainsi que les propriétés de contact telles que les coefficients de frottement. LE
des conditions aux limites ont été établies pour reproduire le scénario d'impression réel, y compris
gradients de température et contraintes résiduelles.

### 2.1 La géométrie :

#### 2.1.1 dimension et position :

__le plateau :__

Position [m] : {-0.1,-0.01}
longueur [m] : 0.3
hauteur  [m] : 0.01

__la couche de PLA__

Position [m] : {0,0}
longueur [m] : 0.1
hauteur  [m] : 0.006

__la chouche de POM__

Position [m] : {0,0.006}
longueur [m] : 0.1
hauteur  [m] : 0.002

 <figure>
 <img src="images/PLA.png" width="300" />
 </figure>

#### 2.1.2. Assemblage et union :

Un assemblage est formé pour créer la paire de contact entre la couche de PLA et le plateau de l'imprimante,
il servira à établir le contact mécanique et thermique.

 <figure>
 <img src="images/contact.png" width="300" />
 </figure>


### Résultats de simulation 
Dans cette simulation sur COMSOL, nous avons modélisé le phénomène de warping dans les pièces en acétal (POM) et en acide polylactique (PLA) imprimées en 3D. Le warping se produit lorsque la pièce se refroidit après l'impression, ce qui peut entraîner des déformations et des fissures dans la pièce finie. Notre objectif était de prédire et d'analyser le warping dans les pièces en POM et PLA pour améliorer leur qualité. 

Nous avons commencé par créer un modèle géométrique de la pièce que nous voulions imprimer en utilisant les outils de modélisation 3D de COMSOL. (La plaque, une couche de 2.4mm de PLA et une couche de 0.6mm de POM) 

<figure>
 <img src="images/comsol.png" width="300" />
 </figure>


Ensuite, nous avons défini les propriétés thermiques et mécaniques de la matière d'impression, ainsi que les paramètres de l'imprimante, tels que la température de l'extrudeuse et la densité de la matière.  

On a créé une faible adhérence entre la plaque et la première couche en PLA

<figure>
 <img src="images/adhérence.png" width="300" />
 </figure>

<figure>
 <img src="images/pression.png" width="300" />
 </figure>

Nous avons ensuite utilisé les équations de transfert de chaleur pour décrire le processus de refroidissement de la pièce après l'impression. Nous avons également simulé la contraction thermique de la matière d'impression pendant le refroidissement et les contraintes résiduelles dans la pièce finie. 

On fixe la température initiale de PLA à 195°C et le POM à 250°C et la plaque à 70°C, et on applique le refroidissement aux deux couches de la pièce  

<figure>
 <img src="images/initiales.png" width="300" />
 </figure>

<figure>
 <img src="images/temp.png" width="300" />
 </figure>

En utilisant les résultats de la simulation, nous avons analysé le warping dans la pièce en POM et PLA. Nous avons pu identifier les zones les plus sujettes au warping et les paramètres d'impression les plus critiques, tels que la température de l'extrudeuse et la vitesse de refroidissement, qui influencent le plus le warping.  

Grâce à cette simulation sur COMSOL, nous avons pu prédire le warping dans les pièces en POM et PLA avant même de lancer l'impression, et optimiser les paramètres d'impression pour produire des pièces de meilleure qualité, plus rapidement et à moindre coût. La simulation sur COMSOL nous a ainsi permis de gagner du temps et de l'argent en évitant les essais et les erreurs coûteux sur l'imprimante 3D réelle. 

<figure>
 <img src="images/simu.png" width="300" />
 </figure>

<figure>
 <img src="images/simula.png" width="300" />
 </figure>

<figure>
 <img src="images/aad.png" width="300" />
 </figure>

<figure>
 <img src="images/ahawa.png" width="300" />
 </figure>

Un bilame est constitué de la juxtaposition de deux matériaux différents dont le coefficient de dilatation n'est pas le même 

<figure>
 <img src="images/bilame.png" width="300" />
 </figure>

Considérons, par exemple, le matériau 1, sa longueur L0 à la température T0, s’accroit de L−L0 si l’on augmente sa température de T−T0 tel que : 

<figure>
 <img src="images/eq1.png" width="300" />
 </figure>

Avec α le coefficient de dilatation linéaire du matériau à T0. 

Le module d'Young, ou module d'élasticité, est la grandeur qui relie la contrainte normale σ (étirement ou compression) subie par le matériau à la déformation élastique ϵ (allongement ou raccourcissement) qui en résulte. Dans la limite d'élasticité du matériau, la loi de Hooke donne : 

<figure>
 <img src="images/eq2.png" width="300" />
 </figure>

Avec l'allongement relatif :

<figure>
 <img src="images/eq3.png" width="300" />
 </figure>

Au sein du matériau, dans cet assemblage plus complexe, il se crée d'autres contraintes entre les deux matériaux. Ils se déforment collés l'un sur l'autre. 

La contrainte créée par l'élévation de température est proportionnelle au coefficient de dilatation et au module d'Young du matériau : 

<figure>
 <img src="images/eq4.png" width="300" />
 </figure>

L'analyse des contraintes et de la déflexion d'un bilame idéal ont été obtenues en 1865 par Yvon Villarceau 


L'équation générale donnant le rayon de courbure R d'un bilame plat uniformément chauffé d'une température T0 à T en l'absence de forces extérieures est donnée par : 

<figure>
 <img src="images/eq5.png" width="300" />
 </figure>

Avec : 

R0 le rayon de courbure initial à la température T0 

α1 et α2 les coefficients de dilatation des matériaux 1 et 2 (1 pour le matériau le moins extensible) 

E1 et E2 les modules d'Young des deux matériaux 

s1 et s2 les épaisseurs des deux matériaux 

s=s1+s2 l'épaisseur du bilame

Dans la plupart des applications industrielles, les bilames suivent des spécifications standards (DIN 1715 en Europe). Par exemple, l'épaisseur des deux matériaux est souvent la même (s1 / s2 = 1), et en prenant deux matériaux dont le module d'Young est presque le même (E1 / E2 ≈ 1), le calcul du rayon de courbure se simplifie : 

<figure>
 <img src="images/eq6.png" width="300" />
 </figure>

On définit la flexibilité (ou courbure spécifique) par le facteur k 

<figure>
 <img src="images/eq7.png" width="300" />
 </figure>

Pour un bilame plat, maintenu à une extrémité et sans courbure initiale à la température T0 

<figure>
 <img src="images/eq8.png" width="300" />
 </figure>

